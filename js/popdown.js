var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var Popdown = React.createClass({
  render: function () {
    return (
      <div className="popdown-component">

      {this.props.children}
      </div>
    );
  }
});

var App = React.createClass({
  getInitialState: function() { return {}; },
  toggleMenu: function (id) {
    this.setState({
      'activeMenu': this.state.activeMenu === id ? null : id
    });
  },
  render: function () {
    return (
      <div className="application">
        <header>
          <h1>Simple Menu Animation</h1>
          <nav>
            <ul>
              <li onClick={this.toggleMenu.bind(this, 1)}>
                <label>Submenu 1</label>
                <ReactCSSTransitionGroup
                transitionName="popdownanim"
                transitionEnterTimeout={350}
                transitionLeaveTimeout={350}>
                {this.state.activeMenu === 1 ?
                  <Popdown key={1}>
                    <strong>Submenu 1</strong><br/>
                    <a href="http://www.google.com">Google Home</a>
                  </Popdown>
                  : []
                }
                </ReactCSSTransitionGroup>
              </li>
              <li onClick={this.toggleMenu.bind(this, 2)}>
                <label>Submenu 2</label>
                <ReactCSSTransitionGroup
                transitionName="popdownanim"
                transitionEnterTimeout={350}
                transitionLeaveTimeout={350}>
                  {this.state.activeMenu === 2 ?
                    <Popdown key={2}>
                      <strong>Submenu 2</strong>
                      <nav>
                        <ul>
                          <li>Submenu 2 item</li>
                          <li>Submenu 2 item 2</li>
                        </ul>
                      </nav>
                    </Popdown>
                    : []
                  }
                </ReactCSSTransitionGroup>
              </li>
            </ul>
          </nav>
        </header>
        <main>
          Animation Works on Click.
        </main>
      </div>
    );
  }
});

	ReactDOM.render(
      <App />, 
      document.getElementById('app'));